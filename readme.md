#WorkOrders

> A personal approach to creating, monitoring, and completing tasks.

##Usage
###Database requirements
Must create the following directory tree in the parent folder:
	workorder-database
		closed-wo
		onhold-wo

###PHP Setup/Installation
I'm currently using PHP's include test server to develop and use this.

####Module requirements
For Fedora 24, must have xml-php module installed:
	su -c "dnf install php-xml"

##Caveats
This may or may not be vulnerable to XSS and the like.  My usage accounts for this being behind a controlled lan and not exposed to the global internet.  You can do it if you like, but don't blame me if someone slithers a spaghetti noodle out your keyboard and murders your cat.

##License
GNU Affero General Public License v3.0

Basically, its open source and you can never, ever change that.  Attribute to me.

##Todo
1. Implement reading and linking of child work orders from the xml.
1. Implement reading and linking of requisitions from xml (These both require interating though some stuff I don't want to figure out 2016/10/03).

