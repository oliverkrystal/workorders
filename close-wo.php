<!-- HTML Template from https://bitbucket.org/oliverkrystal/dotfile
	 Permission granted for use as learning material and inspiration.
	 Dotfile as a git repo theology applies
-->

<?php $workorder = filter_var($_REQUEST["workorder"], FILTER_SANITIZE_STRING);?> <!-- Pull any strings passed and sanitize -->

<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<!--need to change this back to having slash at front-->
		<link rel="stylesheet" type = "text/css" href="stylesheet.css"/>
		<link rel="stylesheet" type = "text/css" href="child-stylesheet.css"/>
		<link rel="icon" type="image/png" href="http://soliloquyforthefallen.net/resources/icon.png" />

		<title>Close a Workorder</title>
	</head>

	<body>
		<?php include('headers/header.php'); ?>

		<?php
			$workorderContent = simplexml_load_file("workorder-database/".$workorder.".xml");
			$workorderContent->status = "Closed";
			$workorderContent->completedate = date('F d Y', time());
			$CloseStatus = $workorderContent->asXML("workorder-database/".$workorder.".xml");

			if ($CloseStatus)
				echo "<p>Workorder ".$workorder." has been closed.</p>";
			else
				echo "<p>Well darn, close the work order has failed.  Reload to try again ...</p>";
		?>

		<p><?php echo "<a href=view-wo.php?workorder=".$workorder.">"; ?>Return to Viewing Workorder</a></p>

	<!--A footer, if needed, and yes, the placement is awkward but that is HTML5 spec.-->
		<footer>

		</footer>
	</body>
</html>
