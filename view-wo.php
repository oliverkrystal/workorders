<!-- HTML Template from https://bitbucket.org/oliverkrystal/dotfile
	 Permission granted for use as learning material and inspiration.
	 Dotfile as a git repo theology applies
-->

<?php $workorder = filter_var($_REQUEST["workorder"], FILTER_SANITIZE_STRING);?> <!-- Pull any strings passed and sanitize -->

<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<!--need to change this back to having slash at front-->
		<link rel="stylesheet" type = "text/css" href="stylesheet.css"/>
		<link rel="stylesheet" type = "text/css" href="child-stylesheet.css"/>
		<link rel="icon" type="image/png" href="http://soliloquyforthefallen.net/resources/icon.png" />

		<title>Show Work Orders</title>
	</head>

	<body>
		<?php include('headers/header.php'); ?> <!-- Insert the standard handle, reference global file so always consistent without help -->
		<?php include('headers/list-wo.headers.php'); ?>

		<p>
			<table>
				<?php
					$workorderContent = simplexml_load_file("workorder-database/".$workorder.".xml");

					echo "<tr><th>Work Order ID</th><th class='child'>".$workorderContent->id."</th></tr>";
					echo "<tr><th>Description</th><th class='child'>".$workorderContent->description."</th></tr>";
					echo "<tr><th>Start Date</th><th class='child'>".$workorderContent->startdate."</th></tr>";
					echo "<tr><th>Completed Date</th><th class='child'>".$workorderContent->completedate."</th></tr>";
					echo "<tr><th>Status</th><th class='child'>".$workorderContent->status."</th></tr>";
					echo "<tr><th>Comments</th><th class='child'>".$workorderContent->comment."</th></tr>";
				?>
			</table>
		</p>

		<p><a href=<?php echo "close-wo.php?workorder=".$workorder; ?>>Close Work Order</a>


	<!--A footer, if needed, and yes, the placement is awkward but that is HTML5 spec.-->
		<footer>

		</footer>
	</body>
</html>
