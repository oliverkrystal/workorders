<!-- HTML Template from https://bitbucket.org/oliverkrystal/dotfile
	 Permission granted for use as learning material and inspiration.
	 Dotfile as a git repo theology applies
-->

<?php $type = filter_var($_REQUEST["filter"], FILTER_SANITIZE_STRING);?> <!-- Pull any strings passed and sanitize -->

<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<!--need to change this back to having slash at front-->
		<link rel="stylesheet" type = "text/css" href="stylesheet.css"/>
		<link rel="stylesheet" type = "text/css" href="child-stylesheet.css"/>
		<link rel="icon" type="image/png" href="http://soliloquyforthefallen.net/resources/icon.png" />

		<title>Show Work Orders</title>
	</head>

	<body>
		<?php include('headers/header.php'); ?> <!-- Insert the standard handle, reference global file so always consistent without help -->
		<?php include('headers/list-wo.headers.php'); ?>

		<p>
			<table>
				<tr>
					<th>Work Order ID</th>
					<th>Description</th>
					<th>Start Date</th>
					<th>Completed Date</th>
					<th>Comments</th>
				</tr>
				<?php
					$workorderDB = scandir("workorder-database");
					sort($workorderDB, SORT_NUMERIC);

					foreach ($workorderDB as &$workorder){
						if ($workorder == "." || $workorder =="..") /*skip the directory listing bits this thing always pulls */
							continue;
						if (substr($workorder, -3, 3) == "xml"){ /*we always use xml and want to skip folder.  This does so */
							$workorderContent = simplexml_load_file("workorder-database/".$workorder);
						if ($type != "" && strtolower($workorderContent->status) != $type)
							continue; /* If we provide a check paramter, don't show unless its the kind we want, unless we didn't set a filter)*/
					echo "<tr>
							<td><a href='view-wo.php?workorder=".$workorderContent->id."'>".$workorderContent->id."</a></td>";
					echo "	<td>".$workorderContent->description."</td>";
					echo " 	<td>".$workorderContent->startdate."</td>";
					echo " 	<td>".$workorderContent->completedate."</td>";
					echo "	<td>".$workorderContent->comment."</td>";
					echo "</tr>";

						}
					}
					unset($workorder);
				?>
			</table>
		</p>


	<!--A footer, if needed, and yes, the placement is awkward but that is HTML5 spec.-->
		<footer>

		</footer>
	</body>
</html>
